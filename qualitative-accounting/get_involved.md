---
layout: page
title: Get Involved
permalink: /getinvolved/
---

There several ways to get involved in Qualitative Accounting:

* Use one (or more!) of the tools that use the standard
* Produce, or help produce, another tool that uses the standard
* Provide assistence in helping develop further versions of the standard

## Using tools
The simplest way to get involed in Qualitative Accounting is to simply use tools that implement the standard and principles. We do not stipulate the purpose, design and development of these tools in order to allow organisations and developers the freedom to provide the experiences that best suit their needs.

The first tools that implement the standard are currently in development! Watch this space. When a tool has been found to produce or consume Qualitative Accounting data, we will list it on the site with a link, and a brief description of its use / purpose.

## Producing tools
We want to support a healthy ecosystem of data exchange and tool use with the standard. If you're thinking of developing a tool that implements the Qualitative Accounting Standard, please get in touch and we can offer you what assistence we can to help get you going.

## Developing the standard
We want Qualitative Accounting to be an Open Standard, that can evolve to suit the needs of a wide variety of organisations and stakeholders. We're currently only at version {{site.version}} of the standard. So it's early days yet, and this is the most important step! We're bound to have missed something.

We're seeking input from everyone - the public, staff or stakeholders of NPOs and Charities, Open Data experts, etc. All contributions are valuable. Watch this space for links to the space we we'll begin growing the standard.
