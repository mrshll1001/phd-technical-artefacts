---
layout: page
title: About
permalink: /about/
---

The Qualitative Accounting data standard seeks to provide an alternative way to collect and present data about the work efforts and spending of Non-Profit Organisations (NPOs) and Charities.

Presently, the transparency and accountability mechanisms available for the organisations are limited in scope and focus only on spending figures; which are presented to the public without the context required to understand them. This can often lead to misunderstandings about how organisations manage money and their work, and limits the ability of people to see the value of this work beyond that of spending.

We believe that things can be done differently. Digital technologies afford us the opportunity to collect and present a vast multitude of media that demonstrates the important work that happens in NPOs and Charities, in interesting ways. Unfortunately, use of digital technologies in this space has largely been relegated to placing existing NPO and Charity accounts online; with the result that the organisations' work remains just out of view and the goals of transparency and engagement are not necessarily met.

The Qualitative Accounting data standard is therefore an attempt to scaffold the production of digital tools that rethink the accounting process, and can place the spending of organisations in context with work and value. The goal is to facilitate an ecosystem of tools, each producing and exchanging data between themselves, organisations, and the public in order to help demonstrate the value of NPOs and Charities.


*Qualitative Accounting is being developed as part of a [Digital Civics](https://digitalcivics.io/) Phd at Open Lab, Newcastle University.*
