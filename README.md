# PhD Technical Artefacts

This repo collects the technical artefacts that resulted from my MRes and PhD work.

* Accountable &ndash; prototype data visualisation for UK Charity Commission and Local Authority Spending data
* Qualitative Accounting &ndash; a very prototypical "data standard" for encapsulating contextual data around work and spending
* Accounting Scrapbook &ndash; a proof-of-concept Android application designed to faciliate collection and rapid tagging of Qualitative Accounting data items, as well as "sharing" them to other applications
* Rosemary Accounts &ndash; a proof-of-concept PHP web application built in Symfony 3 which was designed to facilitate curation of Qualitative Accounting data and the generation of reports. Accepts input from Accounting Scrapbook.

The research output of my PhD is encapsulated in my thesis available at [thesis.mrshll.uk](https://thesis.mrshll.uk). The thesis also makes reference to relevant papers I published.

This project is archived to represent that these projects are not being maintained.
