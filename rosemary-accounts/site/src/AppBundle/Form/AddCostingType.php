<?php


namespace AppBundle\Form;


use AppBundle\Entity\Post;
use AppBundle\Entity\User;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 *
 */
class AddCostingType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $user = $options['user'];

    $builder->add('child', EntityType::class, array(
      'mapped' => false,
      'label' => "Expense",
      'class' => 'AppBundle:Post',
      'query_builder' => function (\AppBundle\Repository\PostRepository $er) use ($user)
      {
        return $er->createQueryBuilder('p')
        ->select('p')
        ->where('p.reconciled = true')
        ->andWhere('p.financialValue < 0')
        ->andWhere('p.parent IS NULL')
        ->andWhere('p.user = :user')
        ->setParameter('user', $user);
      },
      'choice_label' => 'description',
      'attr' => array('class' => 'browser-default')
    ));
  }

  /**
   * Options resolver http://stackoverflow.com/questions/43092246/symfony-3-passing-variables-into-forms/43092919#43092919
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setRequired('user');
    $resolver->setAllowedTypes('user', array(User::class, 'int'));
  }

}
