<?php

namespace AppBundle\Form;

use AppBundle\Entity\Tag;
use AppBundle\Entity\User;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ExpenseType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $user = $options['user'];

      $builder->add('financialValue', NumberType::class, array(
        'required'=>true,
        'label' => "Amount (enter just the number. Rosemary assumes £ GBP",
        'attr' => array('placeholder' => 'e.g. £9.99')))
      ->add('description', TextareaType::Class, array(
        'required'=>true,
        'label' => "Description",
        'attr'=>array('class'=>'materialize-textarea', 'placeholder'=>"Some words to describe the expense")
      ))
      ->add('dateGiven', DateType::class, array(
        'widget' => 'single_text',
        'format' => 'yyyy-MM-dd',
        'label' => 'Date (yyyy-MM-dd)',
        'attr' => array('class' => 'datepicker')
      ))
      ->add('parent', EntityType::class, array(
        'label' => 'Which fund or income stream would you like to cost this to?',
        'class' => 'AppBundle:Post',
        'query_builder' => function (\AppBundle\Repository\PostRepository $er) use ($user)
        {
          return $er->createQueryBuilder('p')
                  ->select('p')
                  ->from('AppBundle:Customer', 'c')
                  ->where('p.reconciled = true')
                  ->andWhere('p.financialValue > 0')
                  ->andWhere('c.funder = true')
                  ->andWhere('p.customer = c')
                  ->andWhere('p.user = :user')
                  ->setParameter('user', $user);
          },
        'placeholder' => "NONE (ie Unrestricted funds or other)",
        'required' => false,
        'choice_label' => function ($post){return $post->getFundDisplayName();},
        'attr' => array('class' => 'browser-default')
      ))
      ->add('media', FileType::class, array(
        'label'=>" ",
        'mapped'=>false,
        'required'=>false ))
      ->add('tags', TextType::class, array(
        'label' => 'Enter some tags separated by commas and spaces e.g. "one, two, three"',
        'required' => true,
        'mapped' => false,
        'attr' => array('value' => "", 'class' => "tag-field")
      ));


  }

  /**
   * Options resolver http://stackoverflow.com/questions/43092246/symfony-3-passing-variables-into-forms/43092919#43092919
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setRequired('user');
    $resolver->setAllowedTypes('user', array(User::class, 'int'));
  }

}
