<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;



/**
 * Allows user to upload a post with an image
 */
class QuotePostType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('quoteContent', TextType::class, array(
      'label' => "Quote",
      'required' => true
    ))
    ->add('quoteAttribution', TextType::class, array(
      'label' => "Who said this? (e.g. Paula, or the Mayor)",
      'required' => true
    ))
    ->add('dateGiven', DateType::class, array(
        'label' => "Date",
        'required'=>true,
        'widget'=>'single_text',
        'attr'=> array('class' => 'datepicker')
      ))
    ->add('tags', TextType::class, array(
      'label' => 'Enter some tags separated by commas and spaces e.g. "one, two, three"',
      'required' => true,
      'mapped' => false,
      'attr' => array('value' => "", 'class' => "tag-field")
    ));
  }

}
