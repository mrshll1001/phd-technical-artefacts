<?php

namespace AppBundle\Form;

use AppBundle\Entity\Tag;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\User;

/**
 * Form Type for report creation
 */
class CreateReportType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('name', TextType::class, array(
      'required' => true,
      'label' => "Report Name",
      'attr' => array('placeholder'=>"Give the report a name")))
      ->add('scopeStart', DateType::class, array(
        'label' => "Start Date",
        'required' => true,
        'widget' => 'single_text',
        'attr' => array('class' => 'datepicker')))
      ->add('scopeEnd', DateType::class, array(
        'label' => "End Date",
        'required' => true,
        'widget' => 'single_text',
        'attr' => array('class' => 'datepicker')
      ))
      ->add('includeTags', CheckboxType::class, array(
        'label'=>"Include Tags Summary",
        'required' => false
      ))
      ->add('includeIncome', CheckboxType::class, array(
        'label'=>"Include Income Summary",
        'required' => false
      ))
      ->add('includeImages', CheckboxType::class, array(
        'label'=>"Include Images",
        'required' => false
      ))
      ->add('includeExpenses', CheckboxType::class, array(
        'label'=>"Include Expense Summary",
        'required' => false
      ))
      ->add('includeLocations', CheckboxType::class, array(
        'label'=>"Include Events and Locations Summary",
        'required' => false
      ))
      ->add('includeQuotes', CheckboxType::class, array(
        'label'=>"Include Quotes",
        'required' => false
      ))
      ->add('includeGrantWorkSummary', CheckboxType::class, array(
        'label'=>"Include a summary of Work by Grant",
        'required' => false
      ))
      ->add('includeGrantCostSummary', CheckboxType::class, array(
        'label'=>"Include a summary of Costing by Grant",
        'required' => false
      ));
  }

}
