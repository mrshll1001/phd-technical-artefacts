<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


 class ClearIntrayAllType extends AbstractType
 {
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('approveAll', SubmitType::class, array(
                  'attr' => array('class' => "btn green")
    ))
    ->add('removeAll', SubmitType::class, array(
                  'attr' => array('class' => "btn red")
    ));

  }

}
