<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\User;
class ConfigureImportType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $headings = $options["csvHeadings"];
    $map = array();
    $map["NOT USED"] = 9000;

    foreach ($headings as $key => $value)
    {
      $map[$value] = array_search($key, array_keys($headings));
    }

      $builder->add('isIncome', ChoiceType::class, array(
        'choices' => array(
                        'This is all Income' => 0,
                        'This is all Expenses' => 1,
                        'The negatives numbers are Expenses, and the positive numbers are Income' => 2
                      ),
        'expanded' => true,
        'multiple' => false,
        'label'    => "How should we treat money?"
      ))
      ->add('financialValue', ChoiceType::class, array(
        'choices' => $map,
          'attr' => array('class'=>"browser-default"),
          'label' => "Which column describes the Amount? (Money)"
      ))
      ->add('dateFormat', ChoiceType::class, array(
        'expanded'  =>  true,
        'multiple'  =>  false,
        'label'     =>  "What format is the date in? (All formats presume the UK and European formatting of day-month-year)",
        'choices'   =>  array(
                              '01/01/2017' => 'd/m/Y',
                              '01.01.2017' => 'd.m.Y',
                              '01/01/17'   => 'd/m/y',
                              '01.01.17'   => 'd.m.y',
                              '01-Jan-17'  => 'd-F-y'
                            )
      ))
      ->add('dateGiven', ChoiceType::class, array(
        'choices' => $map,
          'attr' => array('class'=>"browser-default"),
          'label' => "Which column describes the Date?"
      ))
      ->add('description', ChoiceType::class, array(
        'choices' => $map,
          'attr' => array('class'=>"browser-default"),
          'label' => "Which column describes the description or extra information?"
      ))
      ->add('customer', ChoiceType::class, array(
        'choices' => $map,
          'attr' => array('class'=>"browser-default"),
          'label' => "Which column describes the Customer or Funder? (Who gave you money)"
      ));


  }

  /**
   * Options resolver http://stackoverflow.com/questions/43092246/symfony-3-passing-variables-into-forms/43092919#43092919
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setRequired('csvHeadings');
    $resolver->setAllowedTypes('csvHeadings', array('array'));
    // $resolver->setRequired('user');
    // $resolver->setAllowedTypes('user', array(User::class, 'int'));
  }

}
