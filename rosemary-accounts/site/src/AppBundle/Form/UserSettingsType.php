<?php
namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use AppBundle\Entity\UserSettings;

class UserSettingsType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
          ->add('timelineEnabled', CheckboxType::class, array(
            'label' => "Enable Live Timeline",
            'required' => false,
          ));
  }
}
