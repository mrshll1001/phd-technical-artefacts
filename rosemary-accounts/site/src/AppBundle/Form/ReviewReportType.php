<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Basic form type that handles three different submit actions
 */
class ReviewReportType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('save_and_keep_private', SubmitType::class, array(
      'label' => 'Save and Keep Private',
      'attr' => array('class' => "btn green")))
      ->add('save_and_make_public', SubmitType::class, array(
        'label' => "Save and Make Public",
        'attr' => array('class' => "btn")
      ))
      ->add('remove', SubmitType::class, array(
        'label' => "Remove",
        'attr' => array('class' => 'btn red')
      ));
  }

}
