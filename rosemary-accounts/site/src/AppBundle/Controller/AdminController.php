<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Tag;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Post;
use AppBundle\Entity\AppToken;
use AppBundle\Entity\MediaReference;
use AppBundle\Entity\UserSettings;
use AppBundle\Entity\Report;


use AppBundle\Form\UpdatePasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Sinergi\Token\StringGenerator;
use Doctrine\Common\Collections\Criteria;
use Aws\S3\S3Client;
use Aws\Credentials\Credentials;
use Aws\Sdk;

use Keboola\Csv\CsvFile;

/**
 * Controller to handle site-wide administration activity
 */
class AdminController extends Controller
{


  public function adminAreaAction(Request $request)
  {
     $user = $this->getUser();

     if ($user->getUsername() === $this->getAdminUsername())
     {

       // Get a list of all users to view
       $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

       return $this->render('AppBundle:admin:page_admin.html.twig', array('userList' => $users));
     } else
     {
       throw new \Exception("Denied access");
     }

  }

  public function editUserAction(Request $request, $username)
  {
    $user = $this->getUser();

    if ($user->getUsername() === $this->getAdminUsername())
    {
      // Get the other user
      $oUser = $this->getDoctrine()->getRepository('AppBundle:User')->findOneByUsername($username);
      if ($oUser !== null)
      {
        $form = $this->createForm(UpdatePasswordType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
          // Retrieve plain password
          $plainPassword = $form['plainPassword']->getData();

          // Generate a per-user salt
          $encodedPassword = $this->get('security.password_encoder')->encodePassword($oUser, $plainPassword);

          // Get doctrine
          $em = $this->getDoctrine()->getManager();

          // Update and store
          $oUser->setPassword($encodedPassword);
          $em->persist($oUser);
          $em->flush();

          echo "Password changed";
        }


        return $this->render('AppBundle:admin:page_edit_user.html.twig', array('oUser' => $oUser, 'form'=>$form->createView()));
      }
    }

    throw new \Exception("Denied access");

  }

  protected function getUser()
  {
    return $this->get('security.token_storage')->getToken()->getUser();
  }

  private function getAdminUsername()
  {
    return "mattmarshall";
  }
}
