<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Endroid\QrCode\QrCode;


class TestController extends Controller
{
    /**
     * Test QR Code generation
     */
    public function qrTestAction(Request $request)
    {
      $qrCode = new QrCode('Life is too short to be generating QR codes');

      // header('Content-Type: '.$qrCode->getContentType());
      echo $qrCode->writeString();
    }
}
