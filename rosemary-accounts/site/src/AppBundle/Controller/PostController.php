<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Post;
use AppBundle\Entity\AppToken;

use AppBundle\Form\BudgetCodeType;
use AppBundle\Form\CustomerType;
use AppBundle\Form\IncomeType;
use AppBundle\Form\ExpenseType;
use AppBundle\Form\GenerateTokenType;
use AppBundle\Form\EditPostType;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\DateType;

use Sinergi\Token\StringGenerator;

/**
 * Designed to handle some actions for post creation that don't require pages in the accounts controller
 */
class PostController extends Controller
{

  public function switchPostVisibilityAction(Request $request, $id, $returnTo)
  {
    // Retrieve post and current user from database
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);

    if ($user === $post->getUser())
    {
      $post->setPublic(!$post->getPublic());
      $em = $this->getDoctrine()->getManager();
      $em->persist($post);
      $em->flush();

      return $this->redirectToRoute($returnTo);
    }


  }

  /**
  * Approves a post by id
  */
  public function approvePostAction(Request $request, $id)
  {
    // Retrieve post and current user from database
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);

    // Confirm that the current user is the owner of the Post
    if ($user === $post->getUser())
    {
      // mark the post as approved
      $post->setReconciled(true);
      $em = $this->getDoctrine()->getManager();
      $em->persist($post);
      $em->flush();
    }
    // redirect to incoming posts again
    return $this->redirectToRoute('incoming_posts');
  }

  /**
  * Removes a post by id
  */
  public function removePostAction(Request $request, $id)
  {
    // Retrieve post and current user from database
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);

    // Confirm that the current user is the owner of the Post
    if ($user === $post->getUser())
    {
      $em = $this->getDoctrine()->getManager();
      $em->remove($post);
      $em->flush();
    }
    // redirect to incoming posts again
    return $this->redirectToRoute('incoming_posts');
  }

  /**
   * Page to edit a post
   *
   */
  public function editIncomingPostAction(Request $request, $id)
  {

    // Perform security stuff
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);

    // Check the user
    if ($user === $post->getUser())
    {
      // Set up the form to edit a post
      $form = $this->createForm(EditPostType::class, $post, array('user'=>$user, 'tagstring'=>$post->getTagString()));

      // Handle the form and edit / redirect
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid())
      {
        $post = $form->getData();
        $post->setReconciled(true);



        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

        // Now that we have the post stored, we can parse tha tag string and update the tags in the system
        $tagParser = $this->get('app.tag_parser');
        $tagParser->parseTagsFromArray($post, explode(',', $form['tags']->getData()));

        return $this->redirectToRoute('incoming_posts');
      }

      // Get the intray for the user
      $intray = $this->getInTrayBadgeNumber($user);


      // Render the template with the form and template
      return $this->render('AppBundle:user/page:page_edit_post.html.twig', array('intray'=>$intray,'form'=>$form->createView(), 'existing_tags'=>$post->getTags() ));
    } else
    {
      // Return to index if it's not their post
      return $this->redirect('index');
    }


  }

  /**
   * Removes the costing from a Post Object
   */
  public function removePostCostingAction(Request $request, $id)
  {
    // Perform security stuff
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $post = $this->getDoctrine()->getRepository('AppBundle:Post')->find($id);

    if ($user === $post->getUser())
    {
      $em = $this->getDoctrine()->getManager();

      // Get parent and remove Post from it
      $parent = $post->getParent();
      $parent->removeChild($post);
      $post->setParent(null);
      $em->persist($post);
      $em->persist($parent);
      $em->flush();

      return $this->redirectToRoute('costing');
    }
    else
    {
      return $this->redirectToRoute('index');
    }

  }

  /**
   * TODO this is a hacky state atm, it needs to be done properly with a Symfony form later and be moved to the accounts controller
   * TODO ALSO, nginx is redirecting the post for some reason (probably due to a https thing) so we've needed to make this a GET request temporarily
   */
  public function addPostCostingAction(Request $request)
  {

    $parentId = $request->request->get('parent-id');
    $childId = $request->request->get('child-id');

    // Perform security stuff
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $parent = $this->getDoctrine()->getRepository('AppBundle:Post')->find($parentId);
    $child = $this->getDoctrine()->getRepository('AppBundle:Post')->find($childId);

    if ($user === $parent->getUser() && $user === $child->getUser())
    {
      $em = $this->getDoctrine()->getManager();
      $parent->addChild($child);
      $child->setParent($parent);

      $em->persist($parent);
      $em->persist($child);
      $em->flush();
      return $this->redirectToRoute('costing');
    }
    else
    {
      return $this->redirectToRoute('index');
    }


  }

  protected function getUser()
  {
    return $this->get('security.token_storage')->getToken()->getUser();
  }

  private function getInTrayBadgeNumber($user)
  {
    return count($this->getDoctrine()->getRepository('AppBundle:Post')->findAllNotReconciled($user));
  }

}
