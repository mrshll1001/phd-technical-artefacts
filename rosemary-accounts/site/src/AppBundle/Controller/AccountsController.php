<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Post;
use AppBundle\Entity\AppToken;
use AppBundle\Entity\MediaReference;
use AppBundle\Entity\UserSettings;
use AppBundle\Entity\Report;


use AppBundle\Form\BudgetCodeType;
use AppBundle\Form\CustomerType;
use AppBundle\Form\IncomeType;
use AppBundle\Form\ExpenseType;
use AppBundle\Form\GenerateTokenType;
use AppBundle\Form\ImagePostType;
use AppBundle\Form\EventPostType;
use AppBundle\Form\QuotePostType;
use AppBundle\Form\UserSettingsType;
use AppBundle\Form\CreateReportType;
use AppBundle\Form\ReviewReportType;
use AppBundle\Form\AddCostingType;
use AppBundle\Form\UpdateProfileType;
use AppBundle\Form\UpdatePasswordType;
use AppBundle\Form\UploadMediaType;
use AppBundle\Form\ConfigureImportType;
use AppBundle\Form\ClearIntrayAllType;
use AppBundle\Form\DownloadBudgetType;
use AppBundle\Form\DeleteAppTokenType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\DateType;

use Sinergi\Token\StringGenerator;
use Doctrine\Common\Collections\Criteria;
use Aws\S3\S3Client;
use Aws\Credentials\Credentials;
use Aws\Sdk;

use Keboola\Csv\CsvFile;
use Endroid\QrCode\QrCode;


/**
 * Controller to handle the accounts actions in the control panel
 */
class AccountsController extends Controller
{


  /**
   * Basic summary and home socket_create_listen
   */
  public function summaryAction(Request $request)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    // Get all the posts
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Sneaky user check for settings objects
    if($user->getUserSettings() === NULL)
    {
      $em = $this->getDoctrine()->getManager();

      // Default enable timeline to false
      $userSettings = new UserSettings();
      $userSettings->setTimelineEnabled(false);
      $em->persist($userSettings);

      $user->setUserSettings($userSettings);


      $em->persist($user);
      $em->flush();
    }

    /* Sneaky update from mediaURI to mediaArray for affected types */
    $mediaPosts = $this->getDoctrine()->getRepository('AppBundle:Post')->findAllWithMedia($user, false);
    $em = $this->getDoctrine()->getManager();

    foreach ($mediaPosts as $post)
    {
        if ( empty($post->getMediaArray()) )
        {
          $media = array();
          $media[] = $post->getMediaURI();
          $post->setMediaArray($media);
          $em->persist($post);
          $em->flush();
        }

    }

    $postSummariser = $this->get('app.post_summariser');

    // Get a summary of the posts
    $summary = $postSummariser->postsSummary($user);
    $tagMap = $postSummariser->mostFrequentTags($user, 10);
    $budgetCodeSpending = $postSummariser->budgetCodeSpending($user);

    return $this->render('AppBundle:user/page:page_summary.html.twig', array('intray'=>$intray, 'summary'=>$summary, 'tag_count'=>$tagMap, 'budget_code_spending' => $budgetCodeSpending));

  }

  /**
   * Import and Export Page
   */
  public function importExportAction(Request $request)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Create both the forms
    $uploadForm = $this->createForm(UploadMediaType::class);
    $downloadForm = $this->createForm(DownloadBudgetType::class);

    // We need to check the forms linearly
    $uploadForm->handleRequest($request);

    if ( ($uploadForm->isSubmitted() && $uploadForm->isValid()) || $request->files->get('media') !== NULL )
    {
      // Upload the file, and store a reference to it, take the id, then pass it to the next url
      $file = $uploadForm['media']->getData();
      $filehash = sha1_file($file);

      // $file->move('/web/upload');

      $storageURL = 'http://thesis.mrshll.uk/sample_data.csv';
      // S3 Guff
      // $s3Uploader = $this->get('app.s3_uploader');
      // $storageURL = $s3Uploader->uploadCSVFile($file);

      // Create an entry in the database for the reference, to allow it to be retrieved later
      $databaseEntry = new MediaReference();
      $databaseEntry->setFilehash($filehash);
      $databaseEntry->setUrl($storageURL);
      $em = $this->getDoctrine()->getManager();
      $em->persist($databaseEntry);
      $em->flush();

      // Redirect to the configure import with the id
      return $this->redirectToRoute('configure-import', array('id'=>$databaseEntry->getId()));
    }

    // Now we check the download form
    $downloadForm->handleRequest($request);

    if ($downloadForm->isSubmitted() && $downloadForm->isValid())
    {


      // Creating CSV files is a bit dirty, so what we actually want to do is use Twig to render a CSV file in a template and return the response as a download
      // Get our data, by getting the user and fetching posts with a financial value based on them
      $user = $this->getUser();
      $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->findAllFinancial($user);
      $response = $this->render('AppBundle:user/export:budget_export.csv.twig', array('posts'=>$posts));

      // Now we have the template rendered, we can set some HTTP headers to tell the browser not to render the page and instead return the rendered template as a file
      $response->headers->set('Content-Type', 'text/csv');
      // $response->headers->set('Content-Type', 'application/force-download');
      $response->headers->set('Content-Disposition', 'attachment; filename="rosemary_export.csv"');

      return $response;
    }

    return $this->render('AppBundle:user/page:page_import-export.html.twig', array('intray'=>$intray, 'import_form'=>$uploadForm->createView(), 'export_form'=>$downloadForm->createView()));
  }

  /**
  * Configure Import Page
  */
  public function configureImportAction(Request $request, $id)
  {
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Retrieve the id of item and the URL
    $media = $this->getDoctrine()->getRepository('AppBundle:MediaReference')->find($id);

    // Open the Remote CSV file and parse
    $csv = file($media->getUrl());
    $data = [];
    foreach ($csv as $line)
    {
      $data[] = str_getcsv($line);
    }
    // Extract the headings from the top of the CSV and remove that row from the (now pure) data
    $headings = $data[0];
    unset($data[0]);
    $displayData = array();
    for ($i=1; $i < 5; $i++) {
      array_push($displayData, $data[$i]);
    }

    // Form handling and creation
    $form = $this->createForm(ConfigureImportType::class, null, array('csvHeadings'=>$headings));
    $form->handleRequest($request);

    // Upon form submission
    if ($form->isSubmitted() && $form->isValid())
    {
      $em = $this->getDoctrine()->getManager();

      try
      {
        // Get the data array and iterate over it
        foreach ($data as $row)
        {
          // For each item, create a post
          $post = new Post();

          // Set each element of the content, if it's greater than zero, by the index indicated by the form data

          if($form["financialValue"]->getData() < 9000)
          {
            // Do some cleaning to prevent stuff like '£' characters get in the way
            $string = $row[$form["financialValue"]->getData()];
            $clean = preg_replace("/[^0-9.-]/", "", $string);

            switch ($form['isIncome']->getData())
            {
              case 0:
              case 2:
                $post->setFinancialValue(floatval($clean));
                break;
              case 1:
                $post->setFinancialValue(0 - abs(floatval($clean)));
                break;
              default:
                // Null
                break;
            }
          }


          if ($form["dateGiven"]->getData() < 9000)
          {
            $dateFormat = $form['dateFormat']->getData();
            $dateGiven = \DateTime::createFromFormat($dateFormat, $row[$form["dateGiven"]->getData()]);
            // This has an extra check on it becqause of a potential null value error for some reason. If the dategiven object is null then just create it with a new datetime and let people fix it in post
            if ($dateGiven === null)
            {
              $post->setDateGiven(new \DateTime());
            } else
            {
              $post->setDateGiven($dateGiven);
            }

            $post->setDateCreated(new \DateTime());
          }

          if ($form["description"]->getData() < 9000)
          {
            $post->setDescription($row[$form['description']->getData()]);
          }

          if ($form["customer"]->getData() < 9000 && !($form["customer"]->getData() === "" || $form["customer"]->getData() === NULL ))
          {
            var_dump($row[$form['customer']->getData()]);
                              // This one is more complicated. First we need to do a search for the user's existing customers or funders via the value of the string
            // $customer = $this->getDoctrine()->getRepository('AppBundle:Customer')->findOneByName($row[$form['customer']->getData()]);
            $customer = $this->getDoctrine()->getRepository('AppBundle:Customer')->findOneByNameAndUser($row[$form['customer']->getData()], $user);

                                        // If they exist, extract them and associate them with the Post.
            if ($customer !== NULL)
            {
              $post->setCustomer($customer);
            } else           // If they don't, create one under the presumption they're a Customer. They can change that later.
            {
              $customer = new Customer();
              $customer->setName($row[$form['customer']->getData()]);
              $customer->setUser($user);
              $customer->setFunder(false);

              $em->persist($customer);

              $post->setCustomer($customer);
            }

          }

          $post->setUser($user);
          $post->setReconciled(false);
          $post->setPublic(false);
          $post->setFromImport(true);


          // Save it
          $em->persist($post);
          $em->flush();

        }
      } catch (Exception $e)
      {
        var_dump($row);
      } finally
      {
        return $this->redirectToRoute('import-export');

      }





    }

    return $this->render('AppBundle:user/page:page_configure-import.html.twig', array('intray'=>$intray, 'csv_headings'=>$headings, 'csv_data'=>$displayData, 'mapping_form'=>$form->createView()));
  }

  /**
   * Costing page
   */
  public function costingAction(Request $request)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Get the user's funders and uncosted expenses
    $funders = $this->getDoctrine()->getRepository('AppBundle:Customer')->findFunders($user);
    $uncosted = $this->getDoctrine()->getRepository('AppBundle:Post')->findAllUncostedExpenses($user);

    // Symfony multi forms in a page malarky
    $funderFormMap = array();
    foreach ($funders as $funder)
    {
      $fundForm['funder'] = $funder;
      $formMapping = array();
      foreach ($funder->getPosts() as $parent)
      {
        $mapping = array();
        $mapping['post'] = $parent;

        $uniqueFormName = "form".$parent->getId();
        $form = $this->get('form.factory')->createNamed($uniqueFormName, AddCostingType::class, $parent, array('user'=>$user));

        $mapping['form'] = $form;
        $mapping['form_view'] = $form->createView();
        array_push($formMapping, $mapping);
      }
      $fundForm['map'] = $formMapping;
      array_push($funderFormMap, $fundForm);
    }

    foreach ($funderFormMap as $fundForm)
    {
      foreach ($fundForm['map'] as $map)
      {
        $map['form']->handleRequest($request);
        if ($map['form']->isSubmitted() &&  $map['form']->isValid())
        {
          // Get the parent from the map
          $parent = $map['post'];
          $child = $map['form']['child']->getData();
          $parent->addChild($child);
          $child->setParent($parent);
          $em = $this->getDoctrine()->getManager();
          $em->persist($child);
          $em->flush();

          return $this->redirectToRoute('costing');
        }
        // $map['form_view'] = $map['form']->createView();
      }

    }

    return $this->render('AppBundle:user/page:page_costing.html.twig', array('intray'=>$intray, 'funders'=>$funders, 'uncosted_posts'=>$uncosted, 'funder_form_map'=>$funderFormMap) );
  }

  /**
   * Settings page
   */
  public function userSettingsAction(Request $request)
  {
    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    // Create the three forms
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    $settingsForm = $this->createForm(UserSettingsType::class, $user->getUserSettings());
    $profileForm = $this->createForm(UpdateProfileType::class, $user);
    $passwordForm = $this->createForm(UpdatePasswordType::class);

    // Handle form submissions
    $settingsForm->handleRequest($request);

    if ($settingsForm->isSubmitted() && $settingsForm->isValid() )
    {
      $user->setUserSettings($settingsForm->getData());

      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();

      return $this->redirectToRoute('user_settings');
    }

    $profileForm->handleRequest($request);

    if ($profileForm->isSubmitted())
    {
      $user = $profileForm->getData();
      // $user->setOrganisationName($profileForm['organisationName']->getData());
      // $user->setOrganisationDescription($profileForm['organisationDescription']->getData());

      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();
      return $this->redirectToRoute('user_settings');
    }

    $passwordForm->handleRequest($request);

    if ($passwordForm->isSubmitted() && $passwordForm->isValid())
    {
      // Retrieve plain password
      $plainPassword = $passwordForm['plainPassword']->getData();

      // Generate a per-user salt
      $encodedPassword = $this->get('security.password_encoder')->encodePassword($user, $plainPassword);

      // Get doctrine
      $em = $this->getDoctrine()->getManager();

      // Update and store
      $user->setPassword($encodedPassword);
      $em->persist($user);
      $em->flush();

      // Just for safety, redirect to logout
      return $this->redirectToRoute('logout');
    }


    return $this->render('AppBundle:user/page:page_settings.html.twig', array('intray'=>$intray,
            'settingsForm' => $settingsForm->createView(),
            'profileForm' => $profileForm->createView(),
            'passwordForm' => $passwordForm->createView()
          ));
  }

  /**
   * Presents a list of posts
   */
  public function myPostsAction(Request $request)
  {
    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    $posts = array_reverse($this->getDoctrine()->getRepository('AppBundle:Post')->findByUser($user));

    return $this->render('AppBundle:user/page:page_my-posts.html.twig', array('intray'=>$intray, 'posts'=>$posts));

  }

  /**
   * Page that collects all the unreconciled accouints
   */
  public function incomingPostsAction(Request $request)
  {
    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    // Get the user and all their unreconciled posts
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->findAllNotReconciled($user);
    $postsFromApp = $this->getDoctrine()->getRepository('AppBundle:Post')->findAllNotReconciledFromApp($user);
    $postsFromImport = $this->getDoctrine()->getRepository('AppBundle:Post')->findAllNotReconciledFromImport($user);

    $clearAllForm = $this->createForm(ClearIntrayAllType::class);
    $clearAllForm->handleRequest($request);

    if($clearAllForm->isSubmitted() && $clearAllForm->isValid())
    {
      $em = $this->getDoctrine()->getManager();

      if ($clearAllForm->get('approveAll')->isClicked())
      {
        foreach ($posts as $post)
        {
            $post->setReconciled(true);
            $em->persist($post);
            $em->flush();
        }
      } else if ($clearAllForm->get('removeAll')->isClicked())
      {
        foreach ($posts as $post)
        {
            $em->remove($post);
            $em->flush();
        }
      }


      return $this->redirectToRoute('incoming_posts');

    }

    //TODO Construct the form for removing posts here, with a variable for route to return to as a hidden input. Then apply the form in the card under an if statement

    return $this->render('AppBundle:user/page:page_incoming-posts.html.twig', array('intray'=>$intray, 'posts_all' => $posts, 'posts_app' => $postsFromApp, 'posts_import' => $postsFromImport, 'clear_all_form' => $clearAllForm->createView()));
  }


  public function searchTagsAction(Request $request, $tagstring)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // TODO split the tagstring, and find all tags with that name (and user), and then retrieve their posts
    $postRepository = $this->getDoctrine()->getRepository('AppBundle:Post');
    $tagRepository = $this->getDoctrine()->getRepository('AppBundle:Tag');
    $tags = array();

    foreach(explode(',', $tagstring) as $tagName)
    {
      array_push($tags, $tagRepository->findOneByName($tagName));
    }

    $posts = array();

    foreach ($tags as $tag)
    {
      array_push($posts, $postRepository->createQueryBuilder('p')->where(':tag MEMBER OF p.tags')->setParameter('tag', $tag)->getQuery()->getResult());
    }

    // var_dump($posts);

    return $this->render('AppBundle:user/page:page_search-tags.html.twig', array('intray'=>$intray, 'posts'=>$posts, 'searchTerm'=>$tagstring));
  }

  public function viewExpenseAction(Request $request)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Get all posts which belong to the user, AND have a negative financial value.
    $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->createQueryBuilder('p')->where('p.financialValue < 0')->andWhere('p.user = :user')->setParameter('user', $this->get('security.token_storage')->getToken()->getUser())->getQuery()->getResult();

    $total = 0;
    foreach ($posts as $p)
    {
      $total += $p->getFinancialValue();
    }

    $user = $this->get('security.token_storage')->getToken()->getUser();

    $post = new Post();
    $form = $this->createForm(ExpenseType::class, $post, array('user'=>$user));

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid())
    {
        // Start setting the post things
        $post->setFinancialValue(0 - floatval($form['financialValue']->getData()));
        $post->setDescription($form['description']->getData());
        $post->setDateGiven($form['dateGiven']->getData());
        $post->setDateCreated(new \DateTime());
        $post->setReconciled(true);
        $post->setPublic(false);

        $post->setUser($this->get('security.token_storage')->getToken()->getUser());

        // Do the doctrine thing
        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

        // If the user uploaded an image
        if ($form['media']->getData() !== NULL && $form['media']->getData() !== "")
        {
          // Create an S3 uploader to get the media uri
          $s3Uploader = $this->get('app.s3_uploader');
          $storageURI = $s3Uploader->uploadFile($form['media']->getData());
          $post->setMediaURI($storageURI);

          $mediaRef = new MediaReference();
          $mediaRef->setFilehash(sha1_file($form['media']->getData()));
          $mediaRef->setUrl($storageURI);

          // Persist things
          $em = $this->getDoctrine()->getManager();
          $em->persist($post);
          $em->persist($mediaRef);
          $em->flush();
        }

        // Now that we have the post stored, we can parse tha tag string and update the tags in the system
        $tagParser = $this->get('app.tag_parser');
        $tagParser->parseTagsFromArray($post, explode(',', $form['tags']->getData()));

        return $this->redirectToRoute('expenses');

    }

    // Get Budget codes and suggested tags
    $suggestedTags
     = $this->getDoctrine()->getRepository('AppBundle:Tag')->findSuggestedTags($user);

    return $this->render('AppBundle:user/page:page_view-expense.html.twig', array('intray'=>$intray, 'posts' => $posts, 'total'=>$total, 'form'=>$form->createView(), 'suggestedTags' => $suggestedTags) );
  }

  /**
   * Provides a list of all income
   */
  public function viewIncomeAction(Request $request)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    // Get all posts which belong to the user, AND have a positive financial value.
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->findAllIncome($user);

    // Get the totals overall and per customer
    $postSummariser = $this->get('app.post_summariser');
    $total = $postSummariser->totalIncome($user);
    $customerTotals = $postSummariser->incomeBySource($user);



    // Form Stuff
    $post = new Post();
    $form = $this->createForm(IncomeType::class, $post, array('user'=>$user));

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
      // Begin with the post things
      $post->setFinancialValue(floatval($form['financialValue']->getData()));
      $post->setDateGiven($form['dateGiven']->getData());
      $post->setDateCreated(new \DateTime());
      $post->setCustomer($form['customer']->getData());
      $post->setReconciled(true);
      $post->setUser($this->get('security.token_storage')->getToken()->getUser());
      $post->setPublic(false);

      // Do the doctrine thing
      $em = $this->getDoctrine()->getManager();
      $em->persist($post);
      $em->flush();

      // If the user uploaded an image
      if ($form['media']->getData() !== NULL && $form['media']->getData() !== "")
      {
        // Create an S3 uploader to get the media uri
        $s3Uploader = $this->get('app.s3_uploader');
        $storageURI = $s3Uploader->uploadFile($form['media']->getData());
        $post->setMediaURI($storageURI);

        $mediaRef = new MediaReference();
        $mediaRef->setFilehash(sha1_file($form['media']->getData()));
        $mediaRef->setUrl($storageURI);

        // Persist things
        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->persist($mediaRef);
        $em->flush();
      }

      // Now that we have the post stored, we can parse tha tag string and update the tags in the system
      $tagParser = $this->get('app.tag_parser');
      $tagParser->parseTagsFromArray($post, explode(',', $form['tags']->getData()));

      return $this->redirectToRoute('income');

    }


    // Get Budget codes and suggested tags
    $suggestedTags = $this->getDoctrine()->getRepository('AppBundle:Tag')->findSuggestedTags($user);

    return $this->render('AppBundle:user/page:page_view-income.html.twig', array('intray'=>$intray, 'posts' => $posts, 'total'=>$total, 'customerTotals'=>$customerTotals, 'form'=>$form->createView(), 'suggestedTags' => $suggestedTags ) );

  }


  /**
   * This creates a tag with the isBudgetCode() set to True;
   */
  public function budgetCodeAction(Request $request, $tagstring = null)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    // Stuff to handle a form
    $tag = new Tag();
    $form = $this->createForm(BudgetCodeType::class, $tag);
    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid() )
    {
      $tag = $form->getData();
      $tag->setBudgetCode(true);
      $tag->setUser($this->get('security.token_storage')->getToken()->getUser());

      $em = $this->getDoctrine()->getManager();
      $em->persist($tag);
      $em->flush();

      return $this->redirectToRoute('budget_codes');
    }

    // Display the list of current tags that have been marked as budget codes
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    $budgetCodes = $this->getDoctrine()->getRepository('AppBundle:Tag')->findBudgetCodes($user);
    $orderedTags = $this->get('app.post_summariser')->mostFrequentTags($user);

    // Retrieve the requested tags, extracted from the search term
    $requestedTags = null;
    $results = null;

    if ($tagstring !== null)
    {
      $requestedTags = $this->get('app.tag_parser')->retrieveTagsFromTagString($user, $tagstring, "+");
      $results = $this->get('app.post_summariser')->postsByTag($user, $tagstring);
    }

    return $this->render('AppBundle:user:budget_codes.html.twig', array('intray'=>$intray, 'form' => $form->createView(), 'budgetCodes' =>$budgetCodes, 'ordered_tags'=>$orderedTags, 'requested_tags'=>$requestedTags, 'results'=>$results ));

  }

  /**
   * This allows people to add a customer or a funder to the database for use with invoice style posts
   */
  public function customerAction(Request $request)
  {

    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Stuff to handle form
    $customer = new Customer();
    $form = $this->createForm(CustomerType::class, $customer);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
      $customer = $form->getData();
      $customer->setUser($this->get('security.token_storage')->getToken()->getUser());

      $tag = new Tag();
      $tag->setUser($this->get('security.token_storage')->getToken()->getUser());
      $tag->setName($customer->getName());
      $tag->setBudgetCode(false);

      $em = $this->getDoctrine()->getManager();
      $em->persist($customer);
      $em->persist($tag);
      $em->flush();


      return $this->redirectToRoute('customers');
    }

    // Get customers that belong to this particular user
    $customers = $this->getDoctrine()->getRepository('AppBundle:Customer')->createQueryBuilder('c')->where('c.user = :user')->setParameter('user', $this->get('security.token_storage')->getToken()->getUser())->getQuery()->getResult();

    // Render
    return $this->render('AppBundle:user:customers.html.twig', array('intray'=>$intray, 'form'=> $form->createView(),'customers'=> $customers ));

  }

  /**
   * Allows the user to configure additional applications
   */
  public function configureAppsAction(Request $request)
  {
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    $appToken = new AppToken();
    $form = $this->createForm(GenerateTokenType::class, $appToken);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
      $appToken = $form->getData();
      $appToken->setToken(StringGenerator::randomAlnum(10));
      $appToken->setUser($this->get('security.token_storage')->getToken()->getUser());

      $em = $this->getDoctrine()->getManager();
      $em->persist($appToken);
      $em->flush();

      return $this->redirectToRoute('configure_apps');
    }

    // GEt all of the tokens for this user
    $tokens = $this->getDoctrine()->getRepository('AppBundle:AppToken')->createQueryBuilder('t')->where('t.user = :user')->setParameter('user', $this->get('security.token_storage')->getToken()->getUser())->getQuery()->getResult();

    // Test generating a qr code
    $qrCode = new QrCode("I am Rosemary Accounts!!");

    return $this->render('AppBundle:user/page:page_configure-apps.html.twig', array('intray'=>$intray, 'form'=>$form->createView(), 'tokens' => $tokens, 'qr'=>$qrCode->writeString() ));
  }

  /**
  * Removes an app token TODO this should probably be replaced by a form submission at some point test
  */
  public function removeAppTokenAction(Request $request, $id)
  {
    // First, get the token by the id, and check that the user is the owner.
    $user = $this->getUser();
    $token = $this->getDoctrine()->getRepository('AppBundle:AppToken')->find($id);

    if ($token !== NULL && $token->getUser() === $user)
    {
      // Get Doctrine and do the database removal
      $em = $this->getDoctrine()->getManager();
      $em->remove($token);
      $em->flush();

      // Redirect back to the ConfigureApps page
      return $this->redirectToRoute('configure_apps');
    }
  }

  /**
   * Creates a post with quote data
   */
  public function createQuotePostAction(Request $request)
  {
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // TODO Set up the form
    $post = new Post();

    $form = $this->createForm(QuotePostType::class, $post);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
      $post = $form->getData();
      $post->setUser($user);
      $post->setReconciled(true);
      $post->setPublic(false);

      $em = $this->getDoctrine()->getManager();
      $em->persist($post);
      $em->flush();

      // Now that we have the post stored, we can parse tha tag string and update the tags in the system
      $tagParser = $this->get('app.tag_parser');
      $tagParser->parseTagsFromArray($post, explode(',', $form['tags']->getData()));

      return $this->redirectToRoute('post_quote_page');


    }

    $suggestedTags
     = $this->getDoctrine()->getRepository('AppBundle:Tag')->findSuggestedTags($user);

    return $this->render('AppBundle:user/page:page_post_quote.html.twig', array('intray'=>$intray, 'form'=>$form->createView(), 'suggestedTags' => $suggestedTags));


  }

  /**
   * Creates a post with location data
   */
  public function createEventPostAction(Request $request)
  {
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Set up the form
    $post = new Post();

    $form = $this->createForm(EventPostType::class, $post);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
      $post = $form->getData();
      $post->setUser($user);
      $post->setReconciled(true);
      $post->setPublic(false);

      $em = $this->getDoctrine()->getManager();
      $em->persist($post);
      $em->flush();

      // Now that we have the post stored, we can parse tha tag string and update the tags in the system
      $tagParser = $this->get('app.tag_parser');
      $tagParser->parseTagsFromArray($post, explode(',', $form['tags']->getData()));

      return $this->redirectToRoute('post_event_page');

    }
    $suggestedTags
     = $this->getDoctrine()->getRepository('AppBundle:Tag')->findSuggestedTags($user);
    return $this->render('AppBundle:user/page:page_post_event.html.twig', array('intray'=>$intray, 'form'=>$form->createView(), 'suggestedTags' => $suggestedTags));

  }

  /**
   * Creates a post with an Image
   */
  public function createImagePostAction(Request $request)
  {
    // Perform checks
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Create form
    $post = new Post();
    $form = $this->createForm(ImagePostType::class, $post);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
      // Get the description, and dates
      $post->setDescription($form['description']->getData());
      $post->setDateGiven($form['dateGiven']->getData());
      $post->setDateCreated(new \DateTime());
      $post->setUser($user);
      $post->setReconciled(true);
      $post->setPublic(false);


      // Create an S3 uploader to get the media uri
      $s3Uploader = $this->get('app.s3_uploader');
      $storageURI = $s3Uploader->uploadFile($form['media']->getData());
      $post->setMediaArray(array($storageURI));

      // $post->setMediaURI($storageURI); // Legacy

      $mediaRef = new MediaReference();
      $mediaRef->setFilehash(sha1_file($form['media']->getData()));
      $mediaRef->setUrl($storageURI);

      // Persist things
      $em = $this->getDoctrine()->getManager();
      $em->persist($post);
      $em->persist($mediaRef);
      $em->flush();

      // Now that we have the post stored, we can parse tha tag string and update the tags in the system
      $tagParser = $this->get('app.tag_parser');
      $tagParser->parseTagsFromArray($post, explode(',', $form['tags']->getData()));

      // Finally, redirect
      return $this->redirectToRoute('post_image_page');

    }

    $suggestedTags
     = $this->getDoctrine()->getRepository('AppBundle:Tag')->findSuggestedTags($user);
    return $this->render('AppBundle:user/page:page_post_image.html.twig', array('intray'=>$intray, 'form' => $form->createView(), "suggestedTags" => $suggestedTags) );
  }

  /**
   * Allows creation of a report
   */
  public function createNewReportAction(Request $request)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }

    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // TODO the stuff with the report form
    $report = new Report();
    $form = $this->createForm(CreateReportType::class, $report);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
      // Input the data we can
      $report = $form->getData();

      // Set the essentials
      $report->setUser($user);
      $report->setIsPublic(false);
      $report->setIsReviewed(false);
      $report->setDateGenerated(new \DateTime());

      // Get the repository of posts to add to the report
      $posts = $this->getDoctrine()->getRepository('AppBundle:Post')->findAllBetweenDates($user, $report->getScopeStart(), $report->getScopeEnd());

      if (count($posts) <= 0) {
        throw new \Exception("Empty Post Set", 1);

      }

      $em = $this->getDoctrine()->getManager();
      foreach ($posts as $post)
      {
        $report->addPost($post);
        $post->addReport($report);
        $em->persist($post);
      }

      $em->persist($report);
      $em->flush();

      return $this->redirectToRoute('review_report_page', array('id'=>$report->getId()));
    }

    // Find all the user's existing reports anyway
    $reports = $user->getReports();

    // Render the template
    return $this->render('AppBundle:user/page:page_create_report.html.twig', array('intray'=>$intray, 'form'=>$form->createView(), 'reports'=>$reports ));

  }

  /**
   * Allows a user to review a report
   */
  public function reviewReportAction(Request $request, $id)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }


    // Get the report
    $report = $this->getDoctrine()->getRepository('AppBundle:Report')->find($id);

    // Check the report isn't already reviewd
    if ($report->getIsReviewed())
    {
      return $this->redirectToRoute('index');
    }

    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Security checks
    if ($user === $report->getUser())
    {
      // TODO reporty stuff
      $form = $this->createForm(ReviewReportType::class);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        // Handle report modification here
        $em = $this->getDoctrine()->getManager();

        // If user clicked delete button
        if ($form->get('remove')->isClicked())
        {
          $em->remove($report);
          $em->flush();

          return $this->redirectToRoute('create_report_page');
        } else
        {
          // We know that the report has been reviewed, so we're ok to set this.
          $report->setIsReviewed(true);

          // Check if we can make it public
          if ($form->get('save_and_make_public')->isClicked())
          {
            // Make the report public, make all its children public
            $report->setIsPublic(true);

            // Removed by request as more piecemeal appraoch deeemed appropriate
            // foreach ($report->getPosts() as $post)
            // {
            //   $post->setPublic(true);
            //   $em->persist($post);
            // }

          }

          // Persist the Report
          $em->persist($report);
          $em->flush();
          return $this->redirectToRoute('control_summary');
        }

      }

      $reportData = $this->get('app.report_handler')->parseReport($report);

      // Render the template
      return $this->render('AppBundle:user/page:page_review_report.html.twig', array('intray'=>$intray, 'report'=>$report, 'report_data'=>$reportData,'form'=>$form->createView()));
    } else
    {
      return $this->redirectToRoute('index');
    }


  }

  /**
   * User can view a report
   */
  public function viewReportAction(Request $request, $id)
  {
    // Security check
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY'))
    {
      return $this->redirectToRoute('login');
    }

    // Setup check
    if (!$this->get('security.token_storage')->getToken()->getUser()->getIsSetup())
    {
      return $this->redirectToRoute('user_post-registration');
    }


    // Get the report
    $report = $this->getDoctrine()->getRepository('AppBundle:Report')->find($id);
    $user = $this->getUser();
    $intray = $this->getInTrayBadgeNumber($user);

    // Make sure that the report is reviewed and belongs to the user
    if ($user === $report->getUser() && $report->getIsReviewed())
    {
      //TODO render the portions of the report appropriately
      $rh = $this->get('app.report_handler');

      $reportData = $rh->parseReport($report);


      return $this->render('AppBundle:user/page:page_view_report.html.twig', array('intray'=>$intray, 'report'=>$report, 'report_data'=>$reportData));
    }

    return $this->redirectToRoute("control_summary");
  }

  protected function getUser()
  {
    return $this->get('security.token_storage')->getToken()->getUser();
  }

  private function getInTrayBadgeNumber($user)
  {
    return count($this->getDoctrine()->getRepository('AppBundle:Post')->findAllNotReconciled($user));
  }


}
