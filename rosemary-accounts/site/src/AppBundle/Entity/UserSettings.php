<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserSettings
 *
 * @ORM\Table(name="user_settings")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserSettingsRepository")
 */
class UserSettings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="timelineEnabled", type="boolean")
     */
    private $timelineEnabled;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timelineEnabled
     *
     * @param boolean $timelineEnabled
     *
     * @return UserSettings
     */
    public function setTimelineEnabled($timelineEnabled)
    {
        $this->timelineEnabled = $timelineEnabled;

        return $this;
    }

    /**
     * Get timelineEnabled
     *
     * @return bool
     */
    public function getTimelineEnabled()
    {
        return $this->timelineEnabled;
    }
}

