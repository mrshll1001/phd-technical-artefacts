<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportRepository")
 */
class Report
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many reports have one user
     * @ORM\ManyToOne(targetEntity="user", inversedBy="reports")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many Reports have many posts (ie we want posts to be able to belong to more than one Report)
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="reports")
     */
    private $posts;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateGenerated", type="datetime")
     */
    private $dateGenerated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="scopeStart", type="datetime")
     */
    private $scopeStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="scopeEnd", type="datetime")
     */
    private $scopeEnd;

    /**
     * @var bool
     *
     * @ORM\Column(name="includeTags", type="boolean")
     */
    private $includeTags;

    /**
     * @var bool
     *
     * @ORM\Column(name="includeIncome", type="boolean")
     */
    private $includeIncome;

    /**
     * @var bool
     *
     * @ORM\Column(name="includeImages", type="boolean")
     */
    private $includeImages;

    /**
     * @var bool
     *
     * @ORM\Column(name="includeLocations", type="boolean")
     */
    private $includeLocations;

    /**
     * @var bool
     *
     * @ORM\Column(name="includeQuotes", type="boolean")
     */
    private $includeQuotes;

    /**
     * @var bool
     *
     * @ORM\Column(name="includeExpenses", type="boolean")
     */
    private $includeExpenses;

    /**
     * @var bool
     *
     * @ORM\Column(name="includeGrantWorkSummary", type="boolean")
     */
    private $includeGrantWorkSummary;

    /**
     * @var bool
     *
     * @ORM\Column(name="includeGrantCostSummary", type="boolean")
     */
    private $includeGrantCostSummary;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPublic", type="boolean")
     */
    private $isPublic;

    /**
     * @var bool
     * @ORM\Column(name="isReviewed", type="boolean")
     */
    private $isReviewed;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Report
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateGenerated
     *
     * @param \DateTime $dateGenerated
     *
     * @return Report
     */
    public function setDateGenerated($dateGenerated)
    {
        $this->dateGenerated = $dateGenerated;

        return $this;
    }

    /**
     * Get dateGenerated
     *
     * @return \DateTime
     */
    public function getDateGenerated()
    {
        return $this->dateGenerated;
    }

    /**
     * Set scopeStart
     *
     * @param \DateTime $scopeStart
     *
     * @return Report
     */
    public function setScopeStart($scopeStart)
    {
        $this->scopeStart = $scopeStart;

        return $this;
    }

    /**
     * Get scopeStart
     *
     * @return \DateTime
     */
    public function getScopeStart()
    {
        return $this->scopeStart;
    }

    /**
     * Set scopeEnd
     *
     * @param \DateTime $scopeEnd
     *
     * @return Report
     */
    public function setScopeEnd($scopeEnd)
    {
        $this->scopeEnd = $scopeEnd;

        return $this;
    }

    /**
     * Get scopeEnd
     *
     * @return \DateTime
     */
    public function getScopeEnd()
    {
        return $this->scopeEnd;
    }

    /**
     * Set includeTags
     *
     * @param boolean $includeTags
     *
     * @return Report
     */
    public function setIncludeTags($includeTags)
    {
        $this->includeTags = $includeTags;

        return $this;
    }

    /**
     * Get includeTags
     *
     * @return bool
     */
    public function getIncludeTags()
    {
        return $this->includeTags;
    }

    /**
     * Set includeIncome
     *
     * @param boolean $includeIncome
     *
     * @return Report
     */
    public function setIncludeIncome($includeIncome)
    {
        $this->includeIncome = $includeIncome;

        return $this;
    }

    /**
     * Get includeIncome
     *
     * @return bool
     */
    public function getIncludeIncome()
    {
        return $this->includeIncome;
    }

    /**
     * Set includeImages
     *
     * @param boolean $includeImages
     *
     * @return Report
     */
    public function setIncludeImages($includeImages)
    {
        $this->includeImages = $includeImages;

        return $this;
    }

    /**
     * Get includeImages
     *
     * @return bool
     */
    public function getIncludeImages()
    {
        return $this->includeImages;
    }

    /**
     * Set includeLocations
     *
     * @param boolean $includeLocations
     *
     * @return Report
     */
    public function setIncludeLocations($includeLocations)
    {
        $this->includeLocations = $includeLocations;

        return $this;
    }

    /**
     * Get includeLocations
     *
     * @return bool
     */
    public function getIncludeLocations()
    {
        return $this->includeLocations;
    }

    /**
     * Set includeExpenses
     *
     * @param boolean $includeExpenses
     *
     * @return Report
     */
    public function setIncludeExpenses($includeExpenses)
    {
        $this->includeExpenses = $includeExpenses;

        return $this;
    }

    /**
     * Get includeExpenses
     *
     * @return bool
     */
    public function getIncludeExpenses()
    {
        return $this->includeExpenses;
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     *
     * @return Report
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return bool
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\user $user
     *
     * @return Report
     */
    public function setUser(\AppBundle\Entity\user $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\user
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Add post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return Report
     */
    public function addPost(\AppBundle\Entity\Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \AppBundle\Entity\Post $post
     */
    public function removePost(\AppBundle\Entity\Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Set isReviewed
     *
     * @param boolean $isReviewed
     *
     * @return Report
     */
    public function setIsReviewed($isReviewed)
    {
        $this->isReviewed = $isReviewed;

        return $this;
    }

    /**
     * Get isReviewed
     *
     * @return boolean
     */
    public function getIsReviewed()
    {
        return $this->isReviewed;
    }

    /**
     * Set includeGrantWorkSummary
     *
     * @param boolean $includeGrantWorkSummary
     *
     * @return Report
     */
    public function setIncludeGrantWorkSummary($includeGrantWorkSummary)
    {
        $this->includeGrantWorkSummary = $includeGrantWorkSummary;

        return $this;
    }

    /**
     * Get includeGrantWorkSummary
     *
     * @return boolean
     */
    public function getIncludeGrantWorkSummary()
    {
        return $this->includeGrantWorkSummary;
    }

    /**
     * Set includeGrantCostSummary
     *
     * @param boolean $includeGrantCostSummary
     *
     * @return Report
     */
    public function setIncludeGrantCostSummary($includeGrantCostSummary)
    {
        $this->includeGrantCostSummary = $includeGrantCostSummary;

        return $this;
    }

    /**
     * Get includeGrantCostSummary
     *
     * @return boolean
     */
    public function getIncludeGrantCostSummary()
    {
        return $this->includeGrantCostSummary;
    }

    /**
     * Set includeQuotes
     *
     * @param boolean $includeQuotes
     *
     * @return Report
     */
    public function setIncludeQuotes($includeQuotes)
    {
        $this->includeQuotes = $includeQuotes;

        return $this;
    }

    /**
     * Get includeQuotes
     *
     * @return boolean
     */
    public function getIncludeQuotes()
    {
        return $this->includeQuotes;
    }
}
