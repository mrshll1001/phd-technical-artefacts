<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;

use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;
use AppBundle\Entity\User;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Report;

/**
 * Handles the parsing of reports
 */
class ReportHandler
{

  protected $em;

  function __construct(EntityManager $em)
  {
    $this->em = $em;
  }

  /**
  * Parses a report to return an array of data to display
  */
  public function parseReport($report)
  {

    $data = array();

    $data['at_a_glance'] = $this->atAGlanceReport($report);

    if ($report->getIncludeImages())
    {
      $data['gallery'] = $this->produceGallery($report);

    } else
    {
      $data['gallery'] = null;
    }

    if ($report->getIncludeIncome())
    {
      $data['income_summary'] = $this->incomeSummary($report);
    } else
    {
      $data['income_summary'] = null;
    }

    if ($report->getIncludeGrantWorkSummary())
    {
      $data['grant_work_summary'] = $this->grantWorkSummary($report);
    } else
    {
      $data['grant_work_summary'] = null;
    }

    if ($report->getIncludeGrantCostSummary())
    {
      $data['grant_cost_summary'] = $this->grantCostSummary($report);
    } else
    {
      $data['grant_cost_summary'] = null;
    }

    if ($report->getIncludeQuotes())
    {
      $data['quotes'] = $this->quoteSummary($report);
    } else
    {
      $data['quotes'] = null;

    }

    if($report->getIncludeLocations())
    {
      $data['events'] = $this->eventsData($report);
    } else
    {
      $data['events'] = null;
    }

    return $data;
  }

  private function eventsData($report)
  {
    $results = array();

    foreach ($report->getPosts() as $p)
    {
      if (! ($p->getLocationName() === NULL || $p->getLocationName() === "") )
      {
        $mapping = array();
        $mapping['date'] = $p->getDateGiven();
        $mapping['description'] = $p->getDescription();
        $mapping['location'] = $p->getLocationName();

        array_push($results, $mapping);
      }
    }

    return $results;
  }

  private function quoteSummary($report)
  {
    $results = array();
    $results['total'] = 0;
    $results['quotes'] = array();

    foreach( $report->getPosts() as $p )
    {
      if( ! ($p->getQuoteContent() == NULL || $p->getQuoteContent() === "") )
      {
        $results['total'] = $results['total'] + 1;
        array_push($results['quotes'], ['content'=>$p->getQuoteContent(), 'attribution'=>$p->getQuoteAttribution()]);
      }
    }

    return $results;

  }

  private function grantCostSummary($report)
  {
    $results = array();

    // Get all posts in the report, find the costing (parent)
    foreach( $report->getPosts() as $p )
    {
      // Check for parentage (costing)
      if ($p->getParent() !== null)
      {
        // Check array hasn't got a mapping
        if (!isset($results[$p->getParent()->__toString()]))
        {
          $mapping = array();
          $mapping['name'] = $p->getParent()->getDescription();
          $mapping['cost'] = 0.00;

          // Find the name of that costing
          $results[$p->getParent()->__toString()] = $mapping;

        }

          // Just stick the tags in there now
          // Find the tags FROM THE POST and map it to the costing
          $results[$p->getParent()->__toString()]['cost'] = $results[$p->getParent()->__toString()]['cost'] + $p->getFinancialValue();

    }
  }

  return $results;
}

  /**
   * Produces a summary of tags mapped to grants
   */
  private function grantWorkSummary($report)
  {
    $results = array();

    // Get all posts in the report, find their costing
    foreach($report->getPosts() as $p)
    {
      // Check for parentage (costing)
      if ($p->getParent() !== null)
      {
        // Check array hasn't got a mapping
        if (!isset($results[$p->getParent()->__toString()]))
        {
          $mapping = array();
          $mapping['name'] = $p->getParent()->getDescription();
          $mapping['tags'] = array();

          // Find the name of that costing
          $results[$p->getParent()->__toString()] = $mapping;

        }
          // Just stick the tags in there now
          // Find the tags FROM THE POST and map it to the costing
          $results[$p->getParent()->__toString()]['tags'] = array_unique(
                                                              array_merge($results[$p->getParent()->__toString()]['tags'], $p->getTags()->toArray()
                                                            ));


      }
    }
    return $results;
  }

  /**
   * Produces a summary of income by grants received in this time period
   */
  private function incomeSummary($report)
  {
    $results = array();
    $results['items'] = array();
    $total = 0;
    foreach ($report->getPosts() as $p)
    {
      if ($p->getFinancialValue() > 0.00) {
        $total = $total + $p->getFinancialValue();
        array_push($results['items'], $p);
      }
    }
    $results['total'] = $total;

    return $results;

  }


  /**
   * Produces a list of image URIs
   */
  private function produceGallery($report)
  {
    $results = array();

    foreach ($report->getPosts() as $p)
    {

      if ($p->getMediaArray() !== null)
      {
        foreach ($p->getMediaArray() as $uri)
        {
          array_push($results, $uri);
        }
      }
    }

    return $results;

  }

  /**
   * Produces an array of various ataglance type functions
   */
  private function atAGlanceReport($report)
  {

    $results = array();

    if ($report->getIncludeExpenses())
    {
      $results['expenditure_total'] = $this->expenditureTotal($report->getPosts());
      $results['main_costs_summary'] = $this->mainCostsSummary($report->getPosts());

    } else
    {
      $results['expenditure_total'] = null;
      $results['main_costs_summary'] = null;
    }

    $results['work_summary'] = $this->workSummary($report->getPosts());

    if ($report->getIncludeLocations())
    {
      # code...

      $results['location_count'] = $this->countEvents($report);
    } else
    {
      $results['location_count'] = null;
    }


    return $results;
  }

  /**
   * Produces a array of costs
   */
  private function mainCostsSummary($posts)
  {
    $results = array();

    $tags = $this->mostFrequentTags($posts);

    foreach ($tags as $t)
    {
      if ($t->getBudgetCode())
      {
        array_push($results, $t);
      }
    }

    return $results;
  }

  /**
   * Counts events
   */
  private function countEvents($report)
  {
    $count = 0;
    foreach ($report->getPosts() as $p)
    {
      if (! ($p->getLocationLatitude() === NULL) )
      {
        $count = $count + 1;
      }

    }
    return $count;
  }

  /**
   * Operates on a post set to produce a set of descriptor tags
   */
  private function workSummary($posts)
  {
    $tags = $this->mostFrequentTags($posts, 5);

    $results = array();

    // Exclude budget codes
    foreach ($tags as $t)
    {
      if (!$t->getBudgetCode())
      {
        array_push($results, $t);
      }
    }

    return $results;
  }

  /**
   * Returns ordered mapping of tags to number of items
   */
  private function mostFrequentTags($posts, $limit = 0)
  {
    // Get all of the tags
    $tagsArray = array();

    foreach ($posts as $p)
    {
      $tagsArray = array_merge($tagsArray, $p->getTags()->toArray());
    }

    // Get a unique set
    $tagsArray = array_unique($tagsArray);

    // Trim if necessary
    if ($limit > 0)
    {
      $tagsArray = array_slice($tagsArray, 0, $limit);
    }

    // We don't necessarily need to map tags to occurrences within the set here, for now.
    return $tagsArray;
  }

  /**
   * Simple total of all expenditure
   */
  private function expenditureTotal($posts)
  {
    $total = 0;
    foreach ($posts as $p)
    {
      // Only count expenditure
      if ($p->getFinancialValue() <= 0.00)
      {
        $total = $total + $p->getFinancialValue();
      }
    }
      return $total;
  }



}
