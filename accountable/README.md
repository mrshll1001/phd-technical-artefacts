My old MRES Thesis project. Designed to be a technology probe into transparent spending in UK Local Authorities (Councils) and Non-Profits. Findings from building/evaluating the software were later published at [CHI 2016](https://dl.acm.org/citation.cfm?id=2858301).

It's built using an old version of Symfony with some creaky javascript for the visualisations.

There are two scrapers included: Newcastle City Council, and Northumberland County Council. Run scrapers by heading to `/test` and selecting a scraper to run.